#include <stdio.h>

int main() {
    int n;
    printf("Enter size of the array: ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter the elements of array one by one separating with spaces: ");
    for (int k = 0 ; k < n; ++k) {
        scanf("%d", &arr[k]);
    }

    int temp = 0;   //for swaps
    int min_index = 0;
    int min_value = 0;

    for(int i = 0; i < n; ++i) {
        min_value = arr[i];
        min_index = i;
        for (int j = i + 1; j < n; ++j) {
            if (min_value > arr[j]) {
                min_value = arr[j];
                min_index = j;
            }
        }

        temp = arr[min_index];
        arr[min_index] = arr[i];
        arr[i] = temp;
    }
    

    printf("Sorted Array : ");
    for (int i = 0 ; i < n; ++i) {
        printf("%d ", arr[i]);
    }

    printf("\n");
}
