#include <stdio.h>

int main() {
    int n;
    printf("Enter szie of the array: ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter the elements of array one by one separating with spaces: ");
    for (int i = 0 ; i < n; ++i) {
        scanf("%d", &arr[i]);
    }

    int temp = 0;   //for swaps
    // Bubble sort
    for (int i = 0; i < n; ++i) {
        for(int j = 0; j < n - i - 1; ++j) {
            if (arr[j] > arr[j+1]) {
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }

    printf("Sorted Array : ");
    for (int i = 0 ; i < n; ++i) {
        printf("%d ", arr[i]);
    }

    printf("\n");
}