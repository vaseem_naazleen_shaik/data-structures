#include <stdio.h>

void merge_sort(int[], int, int);
void merge(int[], int, int, int);

int sorted_arr[100];

int main() {
    int n;
    printf("Enter szie of the array: ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter the elements of array one by one separating with spaces: ");
    for (int k = 0 ; k < n; ++k) {
        scanf("%d", &arr[k]);
    }

    merge_sort(arr, 0, n - 1);

    printf("Sorted Array : ");
    for (int i = 0 ; i < n; ++i) {
        printf("%d ", arr[i]);
    }

    printf("\n");

}

void merge_sort(int arr[], int lb, int ub) {
    int mid;
    if (lb < ub) {
        mid  = (lb + ub) / 2;
        
        // divide the arrays
        merge_sort(arr, lb, mid);
        merge_sort(arr, mid + 1, ub);

        // merge them in order
        merge(arr, lb, mid, ub);
    }
}

void merge(int arr[], int lb, int mid, int ub) {
    int i = lb, j = mid + 1, k = lb;
      // to store the sorted list of numbers

    while (i <= mid && j <= ub) {
        if(arr[i] <= arr[j]) {
            sorted_arr[k] = arr[i];
            i++;
        }
        else {
            sorted_arr[k] = arr[j];
            j++;
        }
        k++;
    }

    // check whether there are any additional elements in both halfs
    while(j <= ub) {
        sorted_arr[k] = arr[j];
        j++;
        k++;
    }

    while(i <= mid) {
        sorted_arr[k] = arr[i];
        i++;
        k++;
    }

    // copy the sorted array into original array

    for (i = lb; i <= ub; i++) {
        arr[i] = sorted_arr[i];
    }
}
