#include <stdio.h>

int main() {
    int n;
    printf("Enter szie of the array: ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter the elements of array one by one separating with spaces: ");
    for (int k = 0 ; k < n; ++k) {
        scanf("%d", &arr[k]);
    }
    
    int current_value;
    int pos;

    // insertion sort
    for (int i = 0; i < n; ++i) {
      current_value = arr[i];
      pos = i;
      
      while (pos > 0 && arr[pos - 1] > current_value) {
        arr[pos] = arr[pos - 1];
        pos -= 1;
      } 
      arr[pos] = current_value;
    }

    printf("Sorted Array : ");
    for (int i = 0 ; i < n; ++i) {
        printf("%d ", arr[i]);
    }

    printf("\n");

}
