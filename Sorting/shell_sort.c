#include <stdio.h>


int main() {
    int n;
    printf("Enter szie of the array: ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter the elements of array one by one separating with spaces: ");
    for (int k = 0 ; k < n; ++k) {
        scanf("%d", &arr[k]);
    }
    // shell sort
    int temp; 

    for(int gap = n/2; gap >= 1; gap /= 2) {   // decrement gap till 1
        for(int j = gap; j < n; ++j) {          // traversing through the array
            for(int i = j - gap; i >= 0; i -= gap) { // checking gp element & back elements
                if (arr[i + gap] > arr [i]) {
                    break;
                }
                else {
                    temp = arr[i + gap];
                    arr[i + gap] = arr[i];
                    arr[i] = temp;
                }

            }
        }
    }

    printf("Sorted Array : ");
    for (int i = 0 ; i < n; ++i) {
        printf("%d ", arr[i]);
    }

    printf("\n");

    return 0;
}
