#include <stdio.h>

int main() {
    int n;
    printf("Enter szie of the array: ");
    scanf("%d", &n);

    int arr[n];
    printf("Enter the elements of array one by one separating with spaces: ");
    for (int k = 0 ; k < n; ++k) {
        scanf("%d", &arr[k]);
    }

    int temp = 0;   //for swaps
    int swap_counter = -1; // to count the no of swaps in each sort
    int i = 0;
    // Bubble sort
    while (swap_counter != 0) {
        swap_counter = 0;
        for(int j = 0; j < n - i - 1; ++j) {
            if (arr[j] > arr[j+1]) {
                swap_counter += 1;
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
        i += 1;
    }

    printf("Sorted Array : ");
    for (i = 0 ; i < n; ++i) {
        printf("%d ", arr[i]);
    }

    printf("\n");
}
