# Implementing a binary tree using nodes and references (Object Oriented paradigm)


# Root class
class BinaryTree:
    def __init__(self, root):
        self.key = root
        self.leftChild = None
        self.rightChild = None

    def insert_left(self, newNode):
        # if there is no existing left child --> simple add a node to the tree
        if self.leftChild is None:
            self.leftChild = BinaryTree(newNode)
        # Otherwise insert a node and push the existing child down one level in the tree
        else:
            t = BinaryTree(newNode)
            t.leftChild = self.leftChild
            self.leftChild = t

    def insert_right(self, newNode):
        if self.rightChild is None:
            self.rightChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.rightChild = self.rightChild
            self.rightChild = t

    # Accessor methods
    def get_right_child(self):
        return self.rightChild

    def get_left_child(self):
        return self.leftChild

    def set_root(self, root):
        self.key = root

    def get_root(self):
        return self.key


# Driver code
r = BinaryTree('a')
r.insert_left('b')
r.insert_right('c')
print("Level 1:")
print("           {}        ".format(r.get_root()))
print("         /   \\           ")
print("        {}     {}".format(r.get_left_child().get_root(), r.get_right_child().get_root()))
r.get_left_child().insert_right('d')
r.get_right_child().insert_left('e')
r.get_right_child().insert_right('f')
print() # For line gap
print("Level 2:")
print("           {}        ".format(r.get_root()))
print("         /   \\           ")
print("        {}     {}".format(r.get_left_child().get_root(), r.get_right_child().get_root()))
print("       /     /  \\")
print(f"      {r.get_left_child().get_right_child().get_root()}     {r.get_right_child().get_left_child().get_root()}    {r.get_right_child().get_right_child().get_root()}")

'''
Output:
Level 1:
           a
         /   \
        b     c

Level 2:
           a
         /   \
        b     c
       /     /  \
      d     e    f
'''