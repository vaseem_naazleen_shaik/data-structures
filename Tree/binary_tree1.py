# Implemening binary search tree using python [[List of Lists]]


# Recursive definition of Binary tree
def binary_tree(r):
    return [r, [], []]


# Inserting a left child
def insert_left(root, new_branch):
    t = root.pop(1)
    # If list already has smthng in second pos then we shd push it down
    if len(t) > 1:
        root.insert(1, [new_branch, t, []])
    else:
        root.insert(1, [new_branch, [], []])
    return root


# Inserting a right child
def insert_right(root, new_branch):
    t = root.pop(2)
    # If list already has smthng then we shd push it down
    if len(t) > 1:
        root.insert(2, [new_branch, [], t])
    else:
        root.insert(2, [new_branch, [], []])
    return root


# To get a root node of binary tree
def get_root(root):
    return root[0]


# To set a root value
def set_root(root, val):
    root[0] = value


# To get the Left child of root node
def get_left_child(root):
    return root[1]


# To get the Right child of the root node
def get_right_node(root):
    return root[2]

# Driver code
r = binary_tree("a")
insert_left(r, "b")
insert_right(r, "c")
insert_right(get_left_child(r), "d")
insert_left(get_right_node(r), "e")
insert_right(get_right_node(r), "f")

# Above code gives us
'''
               a
            /      \
           b         c
             \     /    \
              d   e      f
'''


