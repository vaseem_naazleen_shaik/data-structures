
## BIG O :

How quickly the run-time grows relative to the input, as the input increases.

### O(1) - Constant time :

This represents an algorithm whose run-time is fixed. No matter how large is the data is, the algorithm will same same runtime.

### Graph:

![o_1_](/uploads/00a98989f81a9ffaa184a0f11874525a/o_1_.JPG)


### O(n) - Linear Time:

An algorithm is said to take linear time, if the running time increases at most linearly with the size of the input.

### Graph :

![O_n_](/uploads/eff3b037ac9c3898273a5a32e9f50045/O_n_.JPG)

### O(n ^ 2) - Quadratic time:

An algorithm with an efficiency of O(N²) increases at twice the relative rate of O(N)

### Graph :

![O_n_2_](/uploads/ddbf328425b116936e82c3968dfc6240/O_n_2_.JPG)

If two nested loops gives us O(N²), it follows that three nested loops gives us O(N³), four nested loops gives us O(N⁴), and so on.

### O(logN) — Logarithmic time:

An O(log n) algorithm is considered highly efficient, as the ratio of the number of operations to the size of the input decreases and tends to zero when n increases. 

### Graph :

![O_logn_](/uploads/b4e605d55e9244497b19ecbf1a729bd8/O_logn_.JPG)

#### Other Info:

* We Often Ignore Multiples
* We Often Ignore All But the Biggest Power
