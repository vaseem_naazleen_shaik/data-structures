/*Doubly linked list:
OPERATIONS:
Insertion- Time complexity - O(n), Space complexity - O(1)
 - before the head time - O(1) 
 - after the tail O(n) 
 - middle or random position in list O(n)
Deletion - Time complexity - O(n), Space complexity - O(1)
 - Deleting the first node
 - Deleting the last node
 - Deleting the intermediate node
 */

#include<stdio.h>
#include<stdlib.h>

//Tyoe declaration of linkde list..
struct DLLNode {
	int data;  //data
	struct DLLNode *next; //next elem
	struct DLLNode *prev; //prev elem
};

struct DLLNode *head = NULL;
//Inserts the node at given position
void DLLInsert(int data, int position) {
	int k = 0;
	struct DLLNode *newNode;
	newNode = (struct DLLNode *)malloc(sizeof(struct DLLNode));
	if(!newNode) {
		printf("Memory Error!");
		return;
	}
    newNode -> data = data;
    if(position == 0) {
    	newNode -> next = head;
    	newNode -> prev = NULL;
    	if(head)
    		(head) -> prev = newNode;
        head = newNode;
        return;
    }
    struct DLLNode *current = head;
    while(k < position && current -> next != NULL) {
    	current = current -> next;
    	k++;
    }
    newNode -> next = current -> next;
    newNode -> prev = current;
    if(current -> next) {
    	(current -> next) -> prev = newNode;
    }
    current -> next = newNode;

    return;
}
// deletes the node at given position
void DLLDelete(int position) {
	struct DLLNode *temp = head;
	int k = 0;
	if(head == NULL) {
		printf("List is empty!");
		return;
	}
	if(position == 0) {
		head = head -> next;
        if (head) {
		(head) -> prev = NULL;
		free(temp);
		return;
	}
	}
	
	struct DLLNode *current = head , *previous;

	while(k < position && current -> next != NULL) {
      current = current -> next;
      k ++;
	}
	if(k != position) {
		printf("Desired position doesn't exist !");
		return;
	}
	previous = current -> prev;
	previous -> next = current -> next;
    if(current -> next) {
    	(current -> next) -> prev = previous;
    }
    free(current);
    return;
}


// prints the linked list
void print() {

    if (head == NULL) {
        printf("List is empty!\n");
        return;
    }
    else {
        struct DLLNode *current = head;
        while (current -> next != NULL) {
            printf("[%d] -> ", current -> data);
            current = current -> next;
        }
        printf("[%d]\n", current -> data);
        return;
    }
}

int main() {
    DLLInsert(10, 0);
    print();             //[10]
	
    DLLInsert(20, 1);
    print();              //[10] -> [20]

    DLLInsert(30, 2);
    print();              //[10] -> [20] -> [30]

    DLLInsert(5, 0);
    print();             //[5] -> [10] -> [20] -> [30]

    DLLDelete(1);
    print();            //[5] -> [20] -> [30]

}

