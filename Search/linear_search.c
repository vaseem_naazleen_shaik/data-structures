#include <stdio.h>

int main() {
    int n;
    printf("Enter helength of the array: ");
    scanf("%d", &n);
    
    int array[n];
    // take input all the array elements
    printf("Enter the elements of array(separting with a white space): ");
    for (int i = 0; i  < n; ++i) {
        scanf("%d", &array[i]);
    }

    int key;
    printf("Enter element to search: ");
    scanf("%d", &key);
    
    // search for the element in the array
    for (int i = 0; i < n; ++i) {
        if (array[i] == key) {
            printf("Element found at location %d\n", i + 1);
            return 0;
        }
    }

    // if element is not found
    printf("Element not found!\n");
    return 1;
}
