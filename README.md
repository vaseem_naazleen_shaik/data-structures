# Data Structures & Algorithms

My Data structures & Algorithms learning..

## Table of contents
- [BIGO Notation](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/BIGO.md)
- [Searching Algorithms](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/tree/master/Search)
- [Sorting Algorithms](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/tree/master/Sorting)
   - [Bubble Sort](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Sorting/bubble_sort.c)
   - [Insertion Sort](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Sorting/insertion_sort.c)
   - [Merge Sort](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Sorting/merge_sort.c)
   - [Selection Sort](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Sorting/selection_sort.c)
   - [Shell sort](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Sorting/shell_sort.c)
- [Linked Lists](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/tree/master/Linked%20Lists)
   - [Singly Linked List](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Linked%20Lists/linked_list.c)
   - [Doubly Linked List](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/blob/master/Linked%20Lists/doubly_linked_list.c)
- [Tree](https://gitlab.com/vaseem_naazleen_shaik/data-structures/-/tree/master/Tree)
  
## Table of Algorithms learnt so far:

|    ALGORITHM   | Basic concept   | O  | Ω |
| :------------:  |:---------------:|:---:|:--:|
| Selection sort |Find the **smallest** element of the array and swap it with the **first** unsorted element in the array  | n^2  | n^2 |
| Bubble sort    | Swap **adjacent pairs** of elements if they are out of order, effectively **bubbling** larger elements to the right and smaller to left| n^2 | n |
| Insertion sort | Proceed once through the array from left-to-right, **shifting** elements as necessary to insert each element into its correct place|n^2 | n |
| Shell sort | sorting pairs of elements **far-apart** from eachother, then progressively reducing the gap b/n elements compared| n ^2 | nlogn|
| Merge sort     | Split the full array into sub arrays then **merge** those sub arrays back together in the correct order| nlogn | nlogn |
| Quick sort     | choose a **pivot**, partition the array based on pivot at last pivot is in sorted psoition then recursively apply thus to sub arrays|  O(n^2) | O(n*log n) |
| Linear search  | **Iterate** across the array from left-to-right, trying to find the target element | n | 1 |
| Binary search  | Given a sorted array, divide & conquer by systematically eliminating half of the remaining elements in the search for the target element| logn | 1 |
